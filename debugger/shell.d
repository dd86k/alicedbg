/// Command shell and interface to debugger.
///
/// Authors: dd86k <dd@dax.moe>
/// Copyright: © dd86k <dd@dax.moe>
/// License: BSD-3-Clause-Clear
module shell;

import adbg;
import adbg.platform;
import adbg.error;
import adbg.include.c.stdio;
import adbg.include.c.stdlib;
import adbg.include.c.stdarg;
import adbg.os.path;
import adbg.utils.strings : adbg_util_expand;
import core.stdc.string;
import common.errormgmt;
import common.cli : opt_syntax;
import common.utils;
import term;

extern (C):

enum { // Shell options
	SHELL_NOCOLORS = 1, /// Disable colors for logger
}

/// 
__gshared int opt_pid;
/// 
__gshared const(char) **opt_file_argv;

/// Application error
enum ShellError {
	none	= 0,
	invalidParameter	= -1,
	invalidCommand	= -2, // or action or sub-command
	unavailable	= -3,
	loadFailed	= -4,
	pauseRequired	= -5,
	alreadyLoaded	= -6,
	missingOption	= -7,
	missingArgument	= -8,
	parse	= -9,	// parse error
	invalidCount	= -10,
	unattached	= -11,
	
	scanMissingType	= -20,
	scanMissingValue	= -21,
	scanInputOutOfRange	= -22,
	scanNoScan	= -23,
	scanInvalidSubCommand	= -24,
	
	crt	= -1000,
	alicedbg	= -1001,
}

const(char) *shell_error_string(int code) {
	switch (code) with (ShellError) {
	case alicedbg:
		return adbg_error_message();
	case invalidParameter:
		return "Invalid command parameter.";
	case invalidCommand:
		return "Invalid command.";
	case unavailable:
		return "Feature unavailable.";
	case loadFailed:
		return "Failed to load file.";
	case pauseRequired:
		return "Debugger needs to be paused for this action.";
	case alreadyLoaded:
		return "File already loaded.";
	case missingOption:
		return "Missing option for command.";
	case missingArgument:
		return "Missing argument.";
	case parse:
		return "Could not parse input.";
	case invalidCount:
		return "Count must be 1 or higher.";
	case unattached:
		return "Need to be attached to a process for this feature.";
	
	case scanMissingType:
		return "Missing type parameter.";
	case scanMissingValue:
		return "Missing value parameter.";
	case scanInputOutOfRange:
		return "Value out of range.";
	case scanNoScan:
		return "No prior scan were initiated.";
	case scanInvalidSubCommand:
		return "Invalid type or subcommand.";
	
	case none:
		return "No error occured.";
	default:
		return "Unknown error.";
	}
}

private __gshared int shellflags;
private __gshared int logflags;
void logerror(const(char) *fmt, ...) {
	va_list args = void;
	va_start(args, fmt);
	logwrite("error", TextColor.red, fmt, args);
}
void logwarn(const(char) *fmt, ...) {
	va_list args = void;
	va_start(args, fmt);
	logwrite("warning", TextColor.yellow, fmt, args);
}
void loginfo(const(char) *fmt, ...) {
	va_list args = void;
	va_start(args, fmt);
	
	logwrite(null, 0, fmt, args);
}
private
void logwrite(const(char) *pre, int color, const(char) *fmt, va_list args) {
	if (pre) {
		if ((shellflags & SHELL_NOCOLORS) == 0)
			concoltext(cast(TextColor)color, stderr);
		fputs(pre, stderr);
		if ((shellflags & SHELL_NOCOLORS) == 0)
			concolrst(stderr);
		fputs(": ", stderr);
	}
	
	vfprintf(stderr, fmt, args);
	putchar('\n');
}

int shell_start(int argc, const(char)** argv) {
Lcommand:
	fputs("(adbg) ", stdout);
	fflush(stdout);
	
	// .ptr is temporary because a slice with a length of 0
	// also make its pointer null.
	char* line = conrdln().ptr;
	
	// Invalid line or CTRL+D
	if (line == null || line[0] == 4)
		return 0;
	
	// External shell command
	if (line[0] == '!') {
		if (line[1]) // has something
			loginfo("Command exited with code %d\n", system(line + 1));
		goto Lcommand;
	}
	
	int e = shell_exec(line);
	if (e)
		logerror(shell_error_string(e));
	goto Lcommand;
}

int shell_exec(const(char) *command) {
	if (command == null) return 0;
	int argc = void;
	char** argv = adbg_util_expand(command, &argc);
	return shell_execv(argc, cast(const(char)**)argv);
}

int shell_execv(int argc, const(char) **argv) {
	if (argc <= 0 || argv == null)
		return 0;
	
	const(char) *ucommand = argv[0];
	immutable(command2_t) *command = shell_findcommand(ucommand);
	if (command == null)
		return ShellError.invalidCommand;
	
	assert(command.entry, "Command missing entry function");
	return command.entry(argc, argv);
}

int shell_spawn(const(char) *exec, const(char) **argv) {
	// Save for restart
	last_spawn_exec = exec;
	last_spawn_argv = argv;
	
	// Spawn process
	process = adbg_debugger_spawn(exec,
		AdbgSpawnOpt.argv, argv,
		0);
	if (process == null)
		return ShellError.alicedbg;
	
	printf("Process '%s' created", exec);
	if (argv && *argv) {
		printf(" with arguments:");
		for (int i; argv[i]; ++i)
			printf(" '%s'", argv[i]);
	}
	putchar('\n');
	
	return shell_setup();
}

int shell_attach(int pid) {
	// Save for restart
	opt_pid = pid;
	
	// Attach to process
	process = adbg_debugger_attach(pid, 0);
	if (process == null)
		return ShellError.alicedbg;
	
	loginfo("Debugger attached.");
	
	return shell_setup();
}

private:
__gshared:

// NOTE: Process management
//       Right now the shell is only capable of dealing with one process
adbg_process_t *process;	/// Process instance
long event_tid;	/// Exception TID

adbg_disassembler_t *disassembler;	/// Disassembler instance

const(char)* last_spawn_exec;
const(char)** last_spawn_argv;

immutable string MODULE_SHELL = "Shell";
immutable string MODULE_DEBUGGER = "Debugger";
immutable string MODULE_DISASSEMBLER = "Disassembler";
immutable string MODULE_OBJECTSERVER = "Object Server";

immutable string CATEGORY_SHELL = "Command-line";
immutable string CATEGORY_PROCESS = "Process management";
immutable string CATEGORY_CONTEXT = "Thread context management";
immutable string CATEGORY_MEMORY = "Memory management";
immutable string CATEGORY_EXCEPTION = "Exception management";

immutable string SECTION_NAME = "NAME";
immutable string SECTION_SYNOPSIS = "SYNOPSIS";
immutable string SECTION_DESCRIPTION = "DESCRIPTION";
immutable string SECTION_NOTES = "NOTES";
immutable string SECTION_EXAMPLES = "EXAMPLES";

struct command2_help_section_t {
	string name;
	string[] bodies;
}
struct command2_help_t {
	// Debugger, Disassembler, Object Server
	string module_;
	// Process management, Memory, etc.
	string category;
	// Short description.
	string description;
	// 
	command2_help_section_t[] sections;
}
struct command2_t {
	string[] names;
	string description;
	string[] synopsis;
	string doc_module;
	string doc_category;
	command2_help_section_t[] doc_sections;
	int function(int, const(char)**) entry;
}
// TODO: New commands
//       - b|breakpoint: Breakpoint management
//       - sym|symbols: Symbol management
immutable command2_t[] shell_commands = [
	//
	// Debugger
	//
	{
		[ "status" ],
		"Get process status.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "Print the status of the debuggee process." ]
			}
		],
		&command_status,
	},
	{
		[ "spawn" ],
		"Spawn a new process into debugger.",
		[ "FILE [ARGS...]" ],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "Spawns a new process from path with the debugger attached." ]
			}
		],
		&command_spawn,
	},
	{
		[ "attach" ],
		"Attach debugger to live process.",
		[ "PID" ],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "Attaches debugger to an existing process by its Process ID." ]
			}
		],
		&command_attach,
	},
	{
		[ "detach" ],
		"Detach debugger from process.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "If the debugger was attached to a live process, detach "~
				"the debugger." ]
			}
		],
		&command_detach,
	},
	{
		[ "restart" ],
		"Restart the debugging process.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "The debugger will be re-attached or the process will be "~
				"killed and respawned." ]
			}
		],
		&command_restart,
	},
	{
		[ "go" ],
		"Continue debugging process.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "The debugger will be re-attached or the process will be "~
				"killed and respawned." ]
			}
		],
		&command_go,
	},
	{
		[ "kill" ],
		"Terminate process.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "The debugger will be re-attached or the process will be "~
				"killed and respawned." ]
			}
		],
		&command_kill,
	},
	{
		[ "stepi" ],
		"Perform an instruction step.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{
				SECTION_DESCRIPTION,
				[ "From a paused state, executes exactly one or more instruction." ]
			}
		],
		&command_stepi,
	},
	//
	// Memory
	//
	{
		[ "m", "memory" ],
		"Dump process memory from address.",
		[ "ADDRESS [LENGTH=64]" ],
		MODULE_DEBUGGER, CATEGORY_MEMORY,
		[
			{
				SECTION_DESCRIPTION,
				[ "Print memory data from address as hexadecimal." ]
			}
		],
		&command_memory,
	},
	{
		[ "maps" ],
		"List memory mapped items.",
		[],
		MODULE_DEBUGGER, CATEGORY_MEMORY,
		[
			{
				SECTION_DESCRIPTION,
				[ "Lists loaded modules and their memory regions." ]
			}
		],
		&command_maps,
	},
	{
		[ "d", "disassemble" ],
		"Disassemble instructions at address.",
		[ "[ADDRESS=PC/EIP/RIP [COUNT=1]]" ],
		MODULE_DEBUGGER, CATEGORY_MEMORY,
		[
			{
				SECTION_DESCRIPTION,
				[ "Invoke the disassembler at the given address.",
				"The debugger will read process memory, if able,"~
				" and will repeat the operation COUNT times."~
				" By default, it will only disassemble one instruction at"~
				" the register value that PC, EIP, or RIP points to." ]
			}
		],
		&command_disassemble,
	},
	{
		[ "scan" ],
		"Scan for value in memory.",
		[ "TYPE VALUE", "show", "reset", ],
		MODULE_DEBUGGER, CATEGORY_MEMORY,
		[
			{
				SECTION_DESCRIPTION,
				[ "Scan memory maps for specified value. "~
				"No writing capability is available at the moment. "~
				"To list last scan results, use 'show' subcommand.",
				"To clear results, use 'reset' subcommand." ]
			}
		],
		&command_scan,
	},
	//
	// Process management
	//
	{
		[ "plist" ],
		"List running programs.",
		[],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{ SECTION_DESCRIPTION,
			[ "List active processes." ]
			}
		],
		&command_plist,
	},
	//
	// Thread management
	//
	{
		[ "t", "thread" ],
		"Manage process threads.",
		[ "list", "TID registers" ],
		MODULE_DEBUGGER, CATEGORY_PROCESS,
		[
			{ SECTION_DESCRIPTION,
			[ "" ]
			}
		],
		&command_thread,
	},
	//
	// Shell
	//
	{
		[ "pwd" ],
		"Print the current working directory.",
		[],
		MODULE_SHELL, CATEGORY_SHELL,
		[
		],
		&command_pwd,
	},
	{
		[ "cd" ],
		"Change current directory.",
		[ "PATH" ],
		MODULE_SHELL, CATEGORY_SHELL,
		[
		],
		&command_cd,
	},
	{
		[ "help" ],
		"Show help or a command's help article.",
		[ "[ITEM]" ],
		MODULE_SHELL, CATEGORY_SHELL,
		[
		],
		&command_help,
	},
	{
		[ "error" ],
		"Show last error in greater details",
		[],
		MODULE_SHELL, CATEGORY_SHELL,
		[
		],
		&command_error,
	},
	{
		[ "version" ],
		"Show Alicedbg version.",
		[],
		MODULE_SHELL, CATEGORY_SHELL,
		[
		],
		&command_version,
	},
	{
		[ "q", "quit" ],
		"Quit shell session.",
		[],
		MODULE_SHELL, CATEGORY_SHELL,
		[
			{
				SECTION_DESCRIPTION,
				[ "Close the shell session along with the debugger and "~
				"application if it was spawned using the debugger." ]
			}
		],
		&command_quit,
	},
];

immutable(command2_t)* shell_findcommand(const(char) *ucommand) {
	debug static immutable(command2_t) ucommand_crash = {
		[ "crash" ],
		null,
		[],
		null, null,
		[],
		&command_crash
	};
	debug if (strcmp(ucommand, ucommand_crash.names[0].ptr) == 0)
		return &ucommand_crash;
	
	// NOTE: Can't use foreach for local var escape
	for (size_t i; i < shell_commands.length; ++i) {
		immutable(command2_t) *cmd = &shell_commands[i];
		
		for (size_t c; c < cmd.names.length; ++c) {
			if (strcmp(ucommand, cmd.names[c].ptr) == 0)
				return cmd;
		}
	}
	
	return null;
}

// After 
int shell_setup() {
	if (adbg_debugger_on_exception(process, &shell_event_exception) ||
		adbg_debugger_on_process_exit(process, &shell_event_process_exit) ||
		adbg_debugger_on_process_continue(process, &shell_event_process_continue))
		return ShellError.alicedbg;
	
	// Open disassembler for process machine type
	disassembler = adbg_disassembler_open(adbg_process_machine(process));
	if (disassembler == null) {
		logwarn("Disassembler not available (%s)",
			adbg_error_message());
	} else if (opt_syntax) {
		if (adbg_disassembler_options(disassembler,
			AdbgDisassemblerOption.syntax, opt_syntax,
			0))
			return ShellError.alicedbg;
	}
	
	return 0;
}

// TODO: Move as common.disassembler module
int shell_disassemble(size_t address, int *opsize,
	char *addressbuf, size_t addresslen,
	char *machinebuf, size_t machinelen,
	const(char) **mnemonic, const(char) **operands) {
	if (addressbuf)
		snprintf(addressbuf, addresslen, "%#zx", address);
	
	// TODO: Maximum opcode size per architecture
	ubyte[OPCODE_BUFSIZE] buffer = void;
	int e = adbg_memory_read(process, address, buffer.ptr, OPCODE_BUFSIZE);
	if (e) return e;
	
	adbg_opcode_t op = void;
	int err = adbg_disassemble(disassembler, &op, buffer.ptr, OPCODE_BUFSIZE);
	
	if (opsize) *opsize = op.size;
	
	if (machinebuf) {
		for (size_t i, b; i < op.size && b < machinelen; ++i) {
			if (i) {
				machinebuf[b++] = ' ';
				if (b >= machinelen) break;
			}
			b += snprintf(machinebuf + b, machinelen - b, "%02x", op.machine[i]);
		}
	}
	
	if (mnemonic) *mnemonic = op.mnemonic;
	if (operands) *operands = op.operands ? op.operands : "";
	
	return err;
}

void shell_event_exception(adbg_process_t *proc, void *udata, adbg_exception_t *exception) {
	int pid = adbg_process_id(proc);
	event_tid = adbg_exception_tid(exception);
	
	printf("* Process %d (thread %lld) stopped\n"~
		"* Reason  : %s ("~ERR_OSFMT~")\n",
		pid, event_tid,
		adbg_exception_name(exception), adbg_exception_orig_code(exception));
	
	// Fault address available, print it
	if (exception.fault_address) {
		printf("	Address : 0x%llx\n", exception.fault_address);
		char[32] machbuf = void;
		const(char)* mnemonic = void, operands = void;
		// Disassembling instruction at fault address passed
		if (shell_disassemble(cast(size_t)exception.fault_address,
			null,
			null, 0,
			machbuf.ptr, 32,
			&mnemonic, &operands) == 0) {
			printf("* Machine : %s\n", machbuf.ptr);
			printf("* Mnemonic: %s %s\n", mnemonic, operands);
		}
	}
	
	// Print callstack, if available
	adbg_thread_t *thread = adbg_thread_new(event_tid);
	if (thread) {
		void *frames = adbg_frame_list(process, thread);
		if (frames) {
			puts("\n* Callstack (WIP):");
			adbg_stackframe_t *frame = void;
			for (size_t i; (frame = adbg_frame_list_at(frames, i)) != null; ++i) {
				printf("%3zu. %llx\n", i, frame.address);
			}
			adbg_frame_list_close(frames);
		}
	}
}
void shell_event_process_exit(adbg_process_t *proc, void *udata, int code) {
	printf("* Process %d exited with code %d\n", adbg_process_id(proc), code);
}
void shell_event_process_continue(adbg_process_t *proc, void *udata, long tid) {
	printf("* Process %d continued\n", adbg_process_id(proc));
}

void shell_event_help(immutable(command2_t) *command) {
	// Print header
	int p = 34; // horizontal alignment padding
	for (size_t i; i < command.names.length; ++i) {
		if (i) {
			printf(", ");
			--p;
		}
		p -= printf("%s", command.names[i].ptr);
	}
	with (command) printf("%*s  %*s\n\nNAME\n ", p, doc_module.ptr, 34, doc_category.ptr);
	for (size_t i; i < command.names.length; ++i) {
		if (i) putchar(',');
		printf(" %s", command.names[i].ptr);
	}
	printf(" - %s\n", command.description.ptr);
	
	if (command.synopsis.length) {
		printf("\n%s\n", SECTION_SYNOPSIS.ptr);
		foreach (s; command.synopsis) {
			printf("  %s %s\n", command.names[$-1].ptr, s.ptr);
		}
	}
	
	enum COL = 72;
	foreach (section; command.doc_sections) {
		printf("\n%s\n", section.name.ptr);
		
		//TODO: Better cut-offs
		//      [0] spacing? remove
		//      [$-1] non-spacing? put dash
		for (size_t i; i < section.bodies.length; ++i) {
			const(char) *b = section.bodies[i].ptr;
			if (i) putchar('\n');
		LPRINT:
			int o = printf("  %.*s\n", COL, b);
			if (o < COL)
				continue;
			b += COL;
			goto LPRINT;
		}
	}
	
	putchar('\n');
}

debug // This is only to test the crash handler
int command_crash(int, const(char) **) {
	void function() fnull;
	fnull();
	return 0;
}

int command_status(int argc, const(char) **argv) {
	puts(adbg_process_status_string(process));
	return 0;
}

//TODO: List per category
//      Comparing could be per pointer or enum
int command_help(int argc, const(char) **argv) {
	if (argc > 1) { // Requesting help article for command
		const(char) *ucommand = argv[1];
		immutable(command2_t) *command = shell_findcommand(ucommand);
		if (command == null)
			return ShellError.invalidParameter;
		
		shell_event_help(command);
		return 0;
	}
	
	enum PADDING = 20;
	static immutable const(char) *liner = "..........................................";
	foreach (cmd; shell_commands) {
		int p;
		for (size_t i; i < cmd.names.length; ++i) {
			if (i) {
				putchar(',');
				++p;
			}
			p += printf(" %s", cmd.names[i].ptr);
		}
		
		printf("  %.*s %s\n", PADDING - p, liner, cmd.description.ptr);
	}
	
	return 0;
}

int command_spawn(int argc, const(char) **argv) {
	if (argc < 2)
		return ShellError.missingArgument;
	
	return shell_spawn(argv[1], argv + 2);
}

int command_attach(int argc, const(char) **argv) {
	if (argc < 2)
		return ShellError.invalidParameter;
	
	return shell_attach(atoi(argv[1]));
}

int command_detach(int argc, const(char) **argv) {
	if (adbg_debugger_detach(process))
		return ShellError.alicedbg;
	
	adbg_disassembler_close(disassembler);
	return 0;
}

int command_restart(int argc, const(char) **argv) {
	switch (process.creation) with (AdbgCreation) {
	case spawned:
		// Terminate first, ignore on error (e.g., already gone)
		adbg_debugger_terminate(process);
		
		// Spawn, shell still messages status
		return shell_spawn(last_spawn_exec, last_spawn_argv);
	case attached:
		// Detach first, ignore on error (e.g., already detached)
		adbg_debugger_detach(process);
		
		// Attach, shell still messages status
		return shell_attach(opt_pid);
	default:
		return ShellError.unattached;
	}
}

int command_go(int argc, const(char) **argv) {
	if (event_tid && adbg_debugger_continue(process, event_tid))
		return ShellError.alicedbg;
	if (adbg_debugger_wait(process))
		return ShellError.alicedbg;
	return 0;
}

int command_kill(int argc, const(char) **argv) {
	if (adbg_debugger_terminate(process))
		return ShellError.alicedbg;
	loginfo("Process killed");
	adbg_disassembler_close(disassembler);
	return 0;
}

// NOTE: Can't simply execute stepi multiple times in a row
int command_stepi(int argc, const(char) **argv) {
	if (adbg_debugger_stepi(process, event_tid))
		return ShellError.alicedbg;
	if (adbg_debugger_wait(process))
		return ShellError.alicedbg;
	return 0;
}

int command_memory(int argc, const(char) **argv) {
	if (argc < 2)
		return ShellError.missingOption;
	
	long uaddress = void;
	if (parse64(&uaddress, argv[1]))
		return ShellError.parse;
	
	int ulength = 64;
	if (argc >= 3) {
		if (parse32(&ulength, argv[2]))
			return ShellError.parse;
		if (ulength <= 0)
			return 0;
	}
	
	ubyte *data = cast(ubyte*)malloc(ulength);
	if (data == null)
		return ShellError.crt;
	
	if (adbg_memory_read(process, cast(size_t)uaddress, data, ulength))
		return ShellError.alicedbg;
	
	enum COLS = 16; /// Columns in bytes
	enum PADD = 12; /// Address padding
	
	// Print column header
	for (int c; c < PADD; ++c)
		putchar(' ');
	putchar(' ');
	putchar(' ');
	for (int c; c < COLS; ++c)
		printf("%2x ", cast(uint)c);
	putchar('\n');
	
	// Print data rows
	size_t count = ulength / COLS;
	for (size_t c; c < count; ++c, uaddress += COLS) {
		// Print address
		printf("%.*llx  ", PADD, uaddress);
		
		// Print data column
		for (size_t i; i < COLS && c * i < ulength; ++i)
			printf("%02x ", data[(c * COLS) + i]);
		
		// Print ascii column
		putchar(' ');
		for (size_t i; i < COLS && c * i < ulength; ++i)
			putchar(asciichar(data[(c * COLS) + i], '.'));
		
		putchar('\n');
	}
	
	return 0;
}

// TODO: optional arg: filter module by name (contains string)
int command_maps(int argc, const(char) **argv) {
	void *maplist = adbg_memory_mapping(process, 0);
	if (maplist == null)
		return ShellError.alicedbg;
	
	puts("Region           Size       T Perm File");
	adbg_memory_map_t *map = void;
	for (size_t i; (map = adbg_memory_mapping_at(maplist, i)) != null; ++i) {
		char[4] perms = void;
		perms[0] = map.access & AdbgMemPerm.read     ? 'r' : '-';
		perms[1] = map.access & AdbgMemPerm.write    ? 'w' : '-';
		perms[2] = map.access & AdbgMemPerm.exec     ? 'x' : '-';
		perms[3] = map.access & AdbgMemPerm.private_ ? 'p' : 's';
		
		char t = void;
		switch (map.type) {
		case AdbgPageUse.resident: t = 'R'; break;
		case AdbgPageUse.fileview: t = 'F'; break;
		case AdbgPageUse.module_:  t = 'M'; break;
		default:                   t = '?';
		}
		
		with (map) printf("%16zx %10zd %c %.4s %s\n",
			cast(size_t)base, size, t, perms.ptr, name.ptr);
	}
	
	adbg_memory_mapping_close(maplist);
	return 0;
}

//TODO: "start,+length" and "start,end" syntax
int command_disassemble(int argc, const(char) **argv) {
	if (process == null)
		return ShellError.unattached;
	if (disassembler == null)
		return ShellError.unavailable;
	
	// TODO: Add default address to PC/RIP
	//       Once Posix side gets thread id matching
	// Need address
	if (argc < 2)
		return ShellError.missingArgument;
	
	long uaddress = void;
	if (parse64(&uaddress, argv[1]))
		return ShellError.parse;
	
	// Number of instruction, default to 10
	int ucount = 10;
	if (argc >= 3) {
		if (parse32(&ucount, argv[2]))
			return ShellError.parse;
		if (ucount <= 0)
			return 0;
	}
	if (ucount < 1)
		return ShellError.invalidCount;
	
	int size = void;
	enum ADSZ = 12; char[ADSZ] address = void;
	enum MASZ = 24; char[MASZ] machine = void;
	const(char) *mnemonic = void;
	const(char) *operands = void;
	int spacing = adbg_disassembler_max_opcode_size(disassembler) * 2;
	assert(spacing > 0);
	for (int c; c < ucount; ++c) {
		printf("%8llx ", uaddress);
		if (shell_disassemble(
			cast(size_t)uaddress, &size,
			address.ptr, ADSZ, machine.ptr, MASZ,
			&mnemonic, &operands)) { // error
			printf("%*s  %s\n", spacing, machine.ptr, adbg_error_message());
			break;
		}
		printf("%*s  %s\t%s\n", spacing, machine.ptr, mnemonic, operands);
		uaddress += size;
	}
	return 0;
}

size_t last_scan_size;
ulong last_scan_data;
adbg_scanner_t *scanner;
size_t last_scan_results;

void scanner_print_results() {
	// super lazy hack
	long mask = void;
	switch (last_scan_size) {
	case 8: mask = 0xffff_ffff_ffff_ffff; break;
	case 7: mask = 0xff_ffff_ffff_ffff; break;
	case 6: mask = 0xffff_ffff_ffff; break;
	case 5: mask = 0xff_ffff_ffff; break;
	case 4: mask = 0xffff_ffff; break;
	case 3: mask = 0xff_ffff; break;
	case 2: mask = 0xffff; break;
	case 1: mask = 0xff; break;
	default:
		puts("fatal: mask fail");
		return;
	}
	//    0000. ffffffffffffffff  18446744073709551615
	puts("No.   Address           Previous              Current");
	size_t count = adbg_scanner_result_count(scanner);
	for (size_t i; i < count; ++i) {
		adbg_scanner_result_t *result = adbg_scanner_result(scanner, i);
		printf("%4zu. %*llx  %*llu  ", i, -16, result.address, -20, result.u64 & mask);
		ulong udata = void;
		if (adbg_memory_read(process, cast(size_t)result.address, &udata, last_scan_size))
			puts("???");
		else
			printf("%llu\n", udata & mask);
	}
}

// parse type for scan/rescan commands
int scanner_parse(const(char) *input, const(char) *type, long *data) {
	assert(input);
	assert(type);
	assert(data);
	
	if (strcmp(type, "byte") == 0 || strcmp(type, "u8") == 0) {
		if (parse64(data, input))
			return ShellError.parse;
		if (*data > ubyte.max)
			return ShellError.scanInputOutOfRange;
		last_scan_size = ubyte.sizeof;
	} else if (strcmp(type, "short") == 0 || strcmp(type, "u16") == 0) {
		if (parse64(data, input))
			return ShellError.parse;
		if (*data > ushort.max)
			return ShellError.scanInputOutOfRange;
		last_scan_size = ushort.sizeof;
	} else if (strcmp(type, "int") == 0 || strcmp(type, "u32") == 0) {
		if (parse64(data, input))
			return ShellError.parse;
		if (*data > uint.max)
			return ShellError.scanInputOutOfRange;
		last_scan_size = uint.sizeof;
	} else if (strcmp(type, "long") == 0 || strcmp(type, "u64") == 0) {
		if (parse64(data, input))
			return ShellError.parse;
		last_scan_size = ulong.sizeof;
	} else
		return ShellError.scanInvalidSubCommand;
	
	return 0;
}

int command_scan(int argc, const(char) **argv) {
	if (argc < 2)
		return ShellError.scanMissingType;
	
	const(char) *atype = argv[1];
	
	// TODO: write subcommand NUMBER VALUE or rely on a memory write command
	if (strcmp(atype, "show") == 0) {
		if (scanner == null)
			return ShellError.scanNoScan;
		
		scanner_print_results();
		return 0;
	} else if (strcmp(atype, "rescan") == 0 || strcmp(atype, "update") == 0) {
		if (scanner == null)
			return ShellError.scanNoScan;
		if (argc < 2)
			return ShellError.scanMissingType;
		if (argc < 3)
			return ShellError.scanMissingValue;
		
		long data = void;
		int e = scanner_parse(argv[2], argv[1], &data);
		if (e)
			return e;
		
		if (adbg_scanner_rescan(scanner, cast(void*)&last_scan_data))
			return ShellError.alicedbg;
		
		size_t count = adbg_scanner_result_count(scanner);
		loginfo("Rescan completed with %zu results, previously %zu.", count, last_scan_results);
		last_scan_results = count;
		return 0;
	} else if (strcmp(atype, "close") == 0) {
		if (scanner == null)
			return 0;
		
		adbg_scanner_close(scanner);
		scanner = null;
		return 0;
	}
	
	if (argc < 3)
		return ShellError.scanMissingValue;
	
	long data = void;
	int e = scanner_parse(argv[2], atype, &data);
	if (e)
		return e;
	if (scanner)
		adbg_scanner_close(scanner);
	
	last_scan_data = data;
	if ((scanner = adbg_scanner_scan(process, &data, last_scan_size, 0)) == null)
		return ShellError.alicedbg;
	
	last_scan_results = adbg_scanner_result_count(scanner);
	loginfo("Scan completed with %zu results.", last_scan_results);
	return 0;
}

int command_plist(int argc, const(char) **argv) {
	void* proclist = adbg_process_list_new();
	if (proclist == null)
		return ShellError.alicedbg;
	
	puts("PID         Name");
	enum BUFFERSIZE = 2048;
	char[BUFFERSIZE] buffer = void;
	adbg_process_t *proc = void;
	for (size_t i; (proc = adbg_process_list_get(proclist, i)) != null; ++i) {
		printf("%10d  ", adbg_process_id(proc));
		if (adbg_process_path(proc, buffer.ptr, BUFFERSIZE)) {
			version (Trace) trace("error: %s", adbg_error_message());
			else            putchar('\n');
			continue;
		}
		puts(buffer.ptr);
	}
	adbg_process_list_close(proclist);
	return 0;
}

int command_thread(int argc, const(char) **argv) {
	if (process == null)
		return ShellError.pauseRequired;
	if (argc < 2)
		return ShellError.missingArgument;
	
	// TODO: Being able to select thread
	//       then prompt can be (adbg [thread 12345])
	
	adbg_thread_t *thread = void;
	void *thrlist = adbg_thread_list_new(process);
	if (thrlist == null)
		return ShellError.alicedbg;
	scope(exit) adbg_thread_list_close(thrlist);
	
	const(char) *action = argv[1];
	// thread list - get a list of threads
	if (strcmp(action, "list") == 0) {
		printf("Threads:");
		for (size_t i; (thread = adbg_thread_list_get(thrlist, i)) != null; ++i) {
			if (i) putchar(',');
			printf(" %lld", adbg_thread_id(thread));
		}
		putchar('\n');
		return 0;
	}
	
	// Else: thread TID subcommand
	if (argc < 3) // thread id
		return ShellError.missingArgument;
	
	// Select thread
	thread = adbg_thread_list_by_id(thrlist, atoi(argv[1]));
	if (thread == null)
		return ShellError.alicedbg;
	
	action = argv[2];
	if (*action == 'r' || strcmp(action, "registers") == 0) {
		int id;
		adbg_register_t *register = void;
		while ((register = adbg_register_by_id(thread, id++)) != null) {
			char[32] dec = void, hex = void;
			adbg_register_format(dec.ptr, 32, register, AdbgRegisterFormat.dec);
			adbg_register_format(hex.ptr, 32, register, AdbgRegisterFormat.hexPadded);
			printf("%*s  0x%*s  %s\n",
				-16, adbg_register_name(register),
				-16, hex.ptr,
				dec.ptr);
		}
		return 0;
	} else if (*action == 's' || strcmp(action, "stack") == 0) {
		if (argc < 4) // stack action
			return ShellError.missingArgument;
		
		action = argv[3];
		if (strcmp(action, "show") == 0) { // show thread id stack
			void *frames = adbg_frame_list(process, thread);
			if (frames == null)
				return ShellError.alicedbg;
			
			adbg_stackframe_t *frame = void;
			for (size_t i; (frame = adbg_frame_list_at(frames, i)) != null; ++i) {
				printf("%3zu. %llx\n", i, frame.address);
			}
			
			adbg_frame_list_close(frames);
			return 0;
		}
	}
	
	return ShellError.invalidCommand;
}

int command_cd(int argc, const(char) **argv) {
	if (argc < 2)
		return ShellError.missingArgument;
	if (adbg_os_chdir(argv[1]))
		return ShellError.alicedbg;
	return 0;
}

int command_pwd(int argc, const(char) **argv) {
	char[4096] b = void;
	const(char) *path = adbg_os_pwd(b.ptr, 4096);
	if (path == null)
		return ShellError.alicedbg;
	puts(path);
	return 0;
}

int command_error(int argc, const(char) **argv) {
	printf(
		"Message : %s\n"~
		"Code    : %d\n"~
		"Caller  : %s:L%d\n",
		adbg_error_message(),
		adbg_error_code(),
		adbg_error_function(), adbg_error_line());
	return 0;
}

int command_version(int argc, const(char) **argv) {
	static immutable string LINE = `Alicedbg `~ADBG_VERSION~` `~TARGET_PLATFORM~`-`~TARGET_OS~`-`~TARGET_ENV;
	puts(LINE.ptr);
	return 0;
}

int command_quit(int argc, const(char) **argv) {
	// Confirm quit
	if (process) {
		printf("Process %d is still running. Quit and terminate? [Y/n] ",
			adbg_process_id(process));
		fflush(stdout);
	Lgetchar:
		int c = getchar();
		switch (c) {
		case 'n', 'N': return 0;
		case 'y', 'Y', '\n': break;
		default: goto Lgetchar;
		}
	}
	
	exit(0);
	return 0;
}